# TrendNxt HTML 5 L3 Hands On Assignment

## Question :
Take a number input (less than 100) from the user and save it to the web storage. Create a web worker to increment the number till it becomes prime. Provide two buttons of ‘Start’ and ‘End’ to start or end the process. Upon pressing ‘end’, the increment stops, or the number becomes prime – whichever occurs first.

## Solution
Please follow the link to view submission as web worker only works when hosted
### https://nikhil-web-wipro.gitlab.io/trendnxt_html_5_l3_hands-on_assignment/

